import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, tap} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  private isConnected: boolean = false;
  connectedUser!: User|null;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
  ) {
    if(localStorage.getItem('token')) {
      this.isConnected = true;
      this.connectedUser = this.jwtHelper.decodeToken<User>(localStorage.getItem('token') as string) as User;
    }
  }

  login(user: any) {
    return this.http.post<any>('http://localhost:3000/users/login', user)
      // .pipe(tap((res) => {
      //     if(res.success) {
      //       this.isConnected = true;
      //       localStorage.setItem('token', res.token);
      //     }
      // }))
      .pipe(map((res) => {
        if(res.success) {
          this.isConnected = true;
          localStorage.setItem('token', res.token);
        }
        return res;
      }));
  }

  logout() {
    this.isConnected = false;
    this.connectedUser = null;
    localStorage.removeItem('token');
  }

  get isLogged() {
    let token = localStorage.getItem('token');

    if(token !== null && this.jwtHelper.isTokenExpired(token)) {
      this.logout();
    }

    return token !== null && this.isConnected;
  }

  get user() {
    let token = localStorage.getItem('token');

    if(token !== null && !this.connectedUser) {
      this.connectedUser = this.jwtHelper.decodeToken(token) as User;
    }

    return this.connectedUser;
  }
}
