import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AuthServiceService} from "../../services/auth-service.service";
import {tap} from "rxjs";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  entityForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthServiceService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.entityForm = this.fb.group({
      username: [null],
      userpassword: [null]
    });
  }

  submit() {
    const user = this.entityForm.value;
    this.authService.login(user)
      .subscribe((res) => {

      });
  }

  getAllUsers() {
    this.userService.findAll()
      .subscribe((res) => {
        console.log(res);
      });
  }

  logout() {
    this.authService.logout();
  }
}
